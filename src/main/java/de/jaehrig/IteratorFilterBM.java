package de.jaehrig;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterators;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.infra.Blackhole;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 * Benchmark to compare a Guava Iterator-Filter vs.
 * a simple for-each loop
 */
@State(Scope.Thread)
public class IteratorFilterBM {

    private List<Integer> list = new ArrayList<>();

    public IteratorFilterBM() {
        Random random = new Random();
        for (int i = 0; i < 1000; ++i) {
            list.add(random.nextInt());
        }
    }


    @Benchmark
    public void testFilter(Blackhole bh) {
        Predicate<Integer> predicate = new Predicate<Integer>() {

            @Override
            public boolean apply(Integer input) {
                return input % 7 == 0;
            }
        };

        for (Iterator<Integer> iter = Iterators.filter(list.iterator(), predicate); iter.hasNext(); ) {
            bh.consume(iter.next());
        }
    }

    @Benchmark
    public void testLoop(Blackhole bh) {
        for (Integer i : list) {
            if (i % 7 == 0) {
                bh.consume(i);
            }
        }
    }

}
