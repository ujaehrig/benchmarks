package de.jaehrig;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.infra.Blackhole;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * benchmark to compare a sorting in-stream vs. a sort-before
 */
@State(Scope.Thread)
public class SortingBM {


    List<Integer> list = new ArrayList<>();

    public SortingBM() {
        Random random = new Random();
        for (int i = 0; i < 1000; ++i) {
            list.add(random.nextInt());
        }
    }


    @Benchmark
    public void testCollectionSort(Blackhole bh) {

        Collections.sort(list);
        final List<Integer> collect = list.stream().map(i -> i + 1).collect(Collectors.toList());
        bh.consume(collect);
    }

    @Benchmark
    public void testStreamSort(Blackhole bh) {
        final List<Integer> collect = list.stream().sorted().map(i -> i + 1).collect(Collectors.toList());
        bh.consume(collect);
    }


}
