package de.jaehrig;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.infra.Blackhole;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.IntStream;

/**
 * Benchmark to compare the speed of insertions into a HashSet and
 * a TreeSet
 */
@State(Scope.Thread)
public class SetImplBM {

    /**
     * Create a HashSet with enough room for 1000 elements (taking into consideration the load-factor of 0.75)
     *
     * @param bh
     */
    @Benchmark
    public void preSizedHashmap(Blackhole bh) {
        final Set<Integer> hashSet = new HashSet<>(1500);
        IntStream.range(0, 1000).forEach(hashSet::add);
        bh.consume(hashSet);
    }

    @Benchmark
    public void defaultHashmap(Blackhole bh) {
        final Set<Integer> hashSet = new HashSet<>();
        IntStream.range(0, 1000).forEach(hashSet::add);
        bh.consume(hashSet);
    }

    @Benchmark
    public void treeset(Blackhole bh) {
        final Set<Integer> treeSet = new TreeSet<>();
        IntStream.range(0, 1000).forEach(treeSet::add);
        bh.consume(treeSet);
    }


}
