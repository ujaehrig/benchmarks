package de.jaehrig;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.infra.Blackhole;

/**
 * benchmark to compare the System.currentTimeMillis() vs. the System.nanoTime()
 *
 */
@State(Scope.Thread)
public class TimingBM {
    @Benchmark
    public void testMilliseconds(Blackhole bh) {
        bh.consume(System.currentTimeMillis());
    }

    @Benchmark
    public void testNano(Blackhole bh) {
        bh.consume(System.nanoTime());
    }

    /*
     * <pre>
     * Benchmark          Mode  Cnt         Score        Error  Units
     * testMilliseconds  thrpt  200  21557024.015 ± 140870.567  ops/s
     * testNano          thrpt  200  24530552.680 ± 123639.817  ops/s
     * </pre>
     */


}
