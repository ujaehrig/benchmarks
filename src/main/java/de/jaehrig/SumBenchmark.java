package de.jaehrig;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterators;
import com.google.common.primitives.Ints;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.infra.Blackhole;

import java.util.*;
import java.util.stream.IntStream;

/**
 * Benchmark to compare various looping methods
 */
@State(Scope.Thread)
public class SumBenchmark {

    private static final int[] intArray = IntStream.range(0, 10_000).toArray();

    @Benchmark
    public void testStreamParallel(Blackhole bh) {
        int sum = Arrays.stream(intArray).parallel().sum();
        bh.consume(sum);
    }

    @Benchmark
    public void testStream(Blackhole bh) {
        int sum = Arrays.stream(intArray).sum();
        bh.consume(sum);
    }

    @Benchmark
    public void testForEachLoop(Blackhole bh) {
        int sum = 0;
        for(int i : intArray) {
            sum += i;
        }
        bh.consume(sum);
    }

    @Benchmark
    public void testForIndexedLoop(Blackhole bh) {
        int sum = 0;
        for(int i = 0; i < intArray.length; ++i) {
            sum += intArray[i];
        }
        bh.consume(sum);
    }

    @Benchmark
    public void testForIterator(Blackhole bh) {
        int sum = 0;
        for(Iterator<Integer> iterator = Ints.asList(intArray).iterator();
            iterator.hasNext(); ) {
            sum += iterator.next();
        }
        bh.consume(sum);
    }

    /*

# Run complete. Total time: 00:26:56

Benchmark                         Mode  Cnt       Score      Error  Units
SumBenchmark.testStreamParallel  thrpt  200   96548,031 ± 6611,025  ops/s
SumBenchmark.testForEachLoop     thrpt  200  333442,138 ± 1156,749  ops/s
SumBenchmark.testForIndexedLoop  thrpt  200  335843,643 ± 1013,508  ops/s
SumBenchmark.testForIterator     thrpt  200   17674,686 ±  270,588  ops/s
SumBenchmark.testStream          thrpt  200   38890,570 ±  740,203  ops/s

     */

}
