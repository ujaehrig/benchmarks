package de.jaehrig;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.infra.Blackhole;

/**
 * benchmark to compare the System.currentTimeMillis() vs. the System.nanoTime()
 *
 */
@State(Scope.Thread)
public class RegexCompile {

    private static final String SIMPLE_STRING = "abc\nde";
    private static final Pattern SIMPLE_PATTERN = Pattern.compile("[^x]");

    @Benchmark
    public void testPrecompiled(Blackhole bh) {
        final Matcher matcher = SIMPLE_PATTERN.matcher(SIMPLE_STRING);
        final boolean b = matcher.find();
        bh.consume(b);
    }

    @Benchmark
    public void testCompileAlways(Blackhole bh) {
        final Pattern pattern = Pattern.compile("[^x]");
        final Matcher matcher = pattern.matcher(SIMPLE_STRING);
        final boolean b = matcher.find();
        bh.consume(b);
    }




    /* with Pattern:
     * <pre>
     * Benchmark                        Mode  Cnt         Score        Error  Units
     * RegexCompile.testCompileAlways  thrpt  200   5879046.029 ± 102397.200  ops/s
     * RegexCompile.testPrecompiled    thrpt  200  18525557.462 ± 222744.553  ops/s
     * </pre>
     */

    /* with Pattern: [^x]
     * <pre>
     * Benchmark                        Mode  Cnt         Score       Error  Units
     * RegexCompile.testCompileAlways  thrpt  200   5146067.446 ± 74896.508  ops/s
     * RegexCompile.testPrecompiled    thrpt  200  20638307.855 ± 77554.178  ops/s
     * </pre>
     */

}
