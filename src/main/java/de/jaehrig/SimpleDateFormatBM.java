package de.jaehrig;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.infra.Blackhole;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * benchmark to compare a SimpleDateFormat#clone() vs a "new SimpleDateFormat()"
 */
@State(Scope.Thread)
public class SimpleDateFormatBM {

    private static final String SDF_FORMAT = "yyyy-MM-dd";
    private static final SimpleDateFormat SDF = new SimpleDateFormat(SDF_FORMAT);

    @Benchmark
    public  void cloneSDF(Blackhole bh) throws ParseException {
        final SimpleDateFormat sdf = (SimpleDateFormat) SDF.clone();
        final Date date = sdf.parse("2016-12-13");
        bh.consume(date);
    }

    @Benchmark
    public  void newSDF(Blackhole bh) throws ParseException {
        final SimpleDateFormat sdf = new SimpleDateFormat(SDF_FORMAT);
        final Date date = sdf.parse("2016-12-13");
        bh.consume(date);
    }

}
